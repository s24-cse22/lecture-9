#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <iostream>

float calculateWallPrice(float width, float length, float height){
    float area = width * height * 2 + length * height * 2;
    float wallPricePerSqFt = 5.25;

    return area * wallPricePerSqFt;
}

float calculateFloorPrice(float width, float length){
    float floorPricePerSqFt = 12;
    return width * length * floorPricePerSqFt;
}

float calculateRoofPrice(float width, float length){
    float roofPricePerSqFt = 15;
    return width * length * roofPricePerSqFt;
}


void displayResult(float wallPrice, float floorPrice, float roofPrice){
    float total = wallPrice + floorPrice + roofPrice;

    std::cout << "Total is: $" << total << std::endl;
}

void displayLaTeX(float wallPrice, float floorPrice, float roofPrice){
    float totalPrice = wallPrice + floorPrice + roofPrice;
    std::cout << "\\documentclass[12pt]{article}" << std::endl;
    std::cout << "\\usepackage[margin=1in]{geometry}" << std::endl;
    std::cout << "\\title{Tool Shed Quote}" << std::endl;
    std::cout << "\\author{}" << std::endl;
    std::cout << "\\begin{document}" << std::endl;
    std::cout << "\\maketitle" << std::endl;
    std::cout << "\\noindent" << std::endl;
    std::cout << "\\begin{tabular}{p{5in} r}" << std::endl;
    std::cout << "\\hline" << std::endl;
    std::cout << "\\textbf{Description} & \\textbf{Price}\\\\" << std::endl;
    std::cout << "\\hline" << std::endl;
    std::cout << "Wall materials & \\$ " << wallPrice << "\\\\" << std::endl;
    std::cout << "Floor materials & \\$ " << floorPrice<< "\\\\" << std::endl;
    std::cout << "Roof materials & \\$ " << roofPrice << "\\\\" << std::endl;
    std::cout << "\\hline" << std::endl;
    std::cout << "\\hline" << std::endl;
    std::cout << "\\textbf{TOTAL} & \\textbf{\\$ " << totalPrice << "}\\\\" << std::endl;
    std::cout << "\\hline" << std::endl;
    std::cout << "\\end{tabular}" << std::endl;
    std::cout << "\\thispagestyle{empty}" << std::endl;
    std::cout << "\\end{document}" << std::endl;
}

#endif