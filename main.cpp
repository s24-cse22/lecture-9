#include <iostream>
#include "functions.h"

using namespace std;


int main(){
    // Create a program that asks the for the dimensions of a toolshed and generates a
    // PDF quote for the customer

    float width;
    float length;
    float height;

    cin >> width;
    cin >> length;
    cin >> height;

    float wallPrice = calculateWallPrice(width, length, height);
    float floorPrice = calculateFloorPrice(width, length);
    float roofPrice = calculateRoofPrice(width, length);

    displayLaTeX(wallPrice, floorPrice, roofPrice);

    return 0;
}