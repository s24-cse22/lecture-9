#include <igloo/igloo.h>
#include "functions.h"

using namespace igloo;


Context(TestWallFunction) {
    Spec(WallTest8x8x8){
        Assert::That(calculateWallPrice(8, 8, 8), EqualsWithDelta(1344, 0.1));
    }
    Spec(WallTest8x6x8){
        Assert::That(calculateWallPrice(8, 6, 8), EqualsWithDelta(1176, 0.1));
    }
};

Context(TestFloorFunction) {
    Spec(FloorTest8x8x8){
        Assert::That(calculateFloorPrice(8, 8), EqualsWithDelta(768, 0.1));
    }
    Spec(FloorTest8x6x8){
        Assert::That(calculateFloorPrice(8, 6), EqualsWithDelta(576, 0.1));
    }
};

Context(TestRoofFunction) {
    Spec(RoofTest8x8x8){
        Assert::That(calculateRoofPrice(8, 8), EqualsWithDelta(960, 0.1));
    }
    Spec(RoofTest8x6x8){
        Assert::That(calculateRoofPrice(8, 6), EqualsWithDelta(720, 0.1));
    }
};

int main(int argc, const char* argv[]){
    TestRunner::RunAllTests(argc, argv);
}





